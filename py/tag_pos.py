#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright @2015 R&D, CINS Inc. (cins.com)
#
# Author: Eric x.sun <followyourheart1211@gmail.com>
#

"""
POS tagger.
"""

import os
import sys
from optparse import OptionParser

import jieba.posseg as pseg

import settings

reload(sys)
sys.setdefaultencoding('utf-8')


def read_input(fd, delimiter):
    for obj in fd:
        yield obj.split(delimiter)


def check_parameters(**kwargs):
    """Check whether the parameters satisfy the conditions.

    Args:
        delimiter: The delimiter between columns.
        indexes: A array of indexes of the content.
        data: The file name of the data.

    Returns:
        A boolean value for representing the status of the checking.
    """

    delimiter = kwargs.get("delimiter", None)
    if delimiter is None:
        msg = [
            "The delimiter is required.",
            "Use '-s' in console mode or 'delimiter=' in func call to set it."
        ]

        print("{0}".format("\n".join(msg)))
        return False

    indexes = kwargs.get('index', 0)
    if indexes is None:
        msg = [
            "The indexes is required.",
            "Use '-i' in console mode or 'index=' in func call to set it."
        ]

        print("{0}".format("\n".join(msg)))
        return False

    data = kwargs.get('data', None)
    if data is not None and not os.path.isfile(data):
        print("The data does not exist: {0}.".format(data))
        return False

    return True


def main(**kwargs):
    if not check_parameters(**kwargs):
        return False

    data = kwargs.get("data", None)
    out = kwargs.get("out", None)

    delimiter = kwargs.get("delimiter", None)
    indexes = kwargs.get("index", None)

    indexes = map(lambda i: int(i)-1, filter(lambda i: i.isdigit(), indexes.split("|")))

    stdin = sys.stdin if data is None else open(data, "rb")
    stdout = sys.stdout if out is None else open(out, "wb")

    delimiter = settings.FIELD_DELIMITER[delimiter] if delimiter in settings.FIELD_DELIMITER.keys() else delimiter

    for obj in read_input(stdin, delimiter):
        empty_line = False

        for i in indexes:
            obj[i] = reduce(
                lambda s, w2: s + " " + w2,
                [w.word + "/" + w.flag for w in pseg.cut(obj[i].strip())],
                ""
            ).strip()

            empty_line = False if obj[i] else True

        if not empty_line:
            stdout.write("{0}\n".format(delimiter.join(obj).strip()))

    if data is not None:
        stdin.close()
    if out is not None:
        stdout.close()

    return True

if __name__ == "__main__":
    parser = OptionParser(usage="%prog -s delimiter -i index_array -d data -o output")

    parser.add_option(
        "-s", "--delimiter",
        help=u"The delimiter between columns, like \001"
    )

    parser.add_option(
        "-i", "--index_array",
        help=u"Array of index in content, that need to been tagged, starts at 1, like \"1|3|4\"."
    )

    parser.add_option(
        "-d", "--data",
        help=u"The file name of the data to be tagged(includes the full path)"
    )

    parser.add_option(
        "-o", "--out",
        help=u"The file name of the cleaned data(includes the full path)"
    )

    if not sys.argv[1:]:
        parser.print_help()
        exit(1)

    (opts, args) = parser.parse_args()

    main(delimiter=opts.delimiter, index=opts.index_array, data=opts.data, out=opts.out)

